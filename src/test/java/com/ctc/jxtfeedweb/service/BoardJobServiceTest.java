package com.ctc.jxtfeedweb.service;

import java.io.IOException;

import javax.xml.bind.JAXBException;

import org.apache.commons.io.IOUtils;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.ctc.jobboard.marshaller.MarshallerHelper;
import com.ctc.jobboard.util.JobBoardHelper;
import com.ctc.jxtfeedweb.domain.JobAdPost;

public class BoardJobServiceTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() throws IOException, JAXBException {
		CloseableHttpClient httpclient =  HttpClients.createDefault();
		String json = JobBoardHelper.readFileToString("/jobpostrequest.json");
		
		//String json = "{\"username\":\"user\",\"referenceNo\":\"00D28000001u4NZ:a012800000udK3kAAE\",\"password\":\"c2cTest1\",\"orgId\":\"00D28000001u4NZ\",\"namespace\":\"\",\"jobListing\":\""
		//		+ "{\"showLocationDetails\":true,\"shortDescription\":\"TEST POST JXT_3TEST POST JXT_3\",\"salary\":{\"showSalaryDetails\":true,\"salaryType\":\"Annual\",\"min\":\"50000\",\"max\":\"80000\",\"additionalText\":\"\"},\"residentsOnly\":false,\"referral\":{\"referralUrl\":\"\",\"hasReferralFee\":false,\"amount\":\"\"},\"referenceNo\":\"00D28000001u4NZ:a012800000udK3kAAE\",\"publicTransport\":\"\",\"listingClassification\":{\"workType\":\"\",\"tags\":\"\",\"streetAddress\":\"\",\"sector\":\"\",\"location\":\"\",\"country\":\"\",\"area\":\"\"},\"jobUrl\":\"111 Test Job\",\"jobTitle\":\"111 Test Job\",\"jobTemplateID\":\"\",\"jobFullDescription\":\"TEST POST JXT_3<br />TEST POST JXT_3<br />TEST POST JXT_3<br />TEST POST JXT_3\",\"jobAdType\":\"Normal\",\"isQualificationsRecognised\":false,\"expiryDate\":null,\"contactDetails\":\"\",\"consultantID\":\"\",\"companyName\":\"PeopleCloud Default\",\"bulletpoints\":{\"bulletPoint3\":\"TEST POST JXT_3\",\"bulletPoint2\":\"TEST POST JXT_3\",\"bulletPoint1\":\"TEST POST JXT_3\"},\"arrayOfCategory\":{\"categories\":null},\"applicationMethod\":{\"jobApplicationType\":\"URL\",\"applicationUrl\":\"https://testament.clicktocloud.com/peoplecloud/GetApplicationForm?jobRefCode=00D28000001u4NZ:a012800000udK3kAAE&website=jxt\",\"applicationEmail\":\"\"},\"advertiserJobTemplateLogoID\":\"\"}"
		//		+ "\",\"jobBoardSfApiName\":\"\",\"jobBoard\":\"JXT\",\"advertiserId\":\"10546\"}";
		
		
		MarshallerHelper.convertJsontoObject(json, JobAdPost.class);
		CloseableHttpResponse response = null;
    	
    	HttpPost httppost = new HttpPost("https://testament.clicktocloud.com/jxtfeedweb/job/postJsonJobAd");
    	
    	try {
    		StringEntity entity = new StringEntity(json);
    		entity.setContentType("application/json");
    		
	    	httppost.setEntity(entity);
	    	
	    	response = httpclient.execute(httppost);
	    	System.out.println( response.toString());
	    	System.out.println( IOUtils.toString(response.getEntity().getContent()));
	    
	    
	    	
    	} catch (Exception e) {
    	
    		e.printStackTrace();
    	} finally {
    		if (response != null) {
    			try {
					response.close();
				} catch (IOException e) {
				
				}
    		}
    		
    		if(httpclient != null){
    			try {
					httpclient.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
    		}
    	}
	}

}
