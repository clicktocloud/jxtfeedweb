package com.ctc.jxtfeedweb.schedule;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.ctc.jobboard.jxt.bulk.JXTClient;


@Component
public class JobPostingSchedule {
	private static Logger logger = Logger.getLogger("com.ctc.jobboard");
	public void bulkPostJobs(){
		
		logger.info("**************** Bulk Post Jobs TO JXT****************");
		
		try {
			
			JXTClient.start();
			
		} catch (Exception e) {
			
			logger.error( e );
		}
		
		logger.info("********************** END **********************");
	}

}
