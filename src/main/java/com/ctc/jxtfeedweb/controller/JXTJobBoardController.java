/**
 * 
 */
package com.ctc.jxtfeedweb.controller;


import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ctc.jobboard.jxt.domain.ext.JobPostResponse;
import com.ctc.jobboard.jxt.service.JXTArchiveJobService;
import com.ctc.jobboard.jxt.service.JXTPostJobService;
import com.ctc.jxtfeedweb.domain.JobAdPost;



@Controller
public class JXTJobBoardController {
	@Autowired
	private HttpServletRequest request;
	
	private static Logger logger = Logger.getLogger("com.ctc.jobboard");
	
	
	@RequestMapping(method=RequestMethod.POST,value="/postJobAd",consumes="application/json;charset=utf-8")
	public String postJobAd(@ModelAttribute JobAdPost jobAdPost,ModelMap model){
		
		try{
			
			logger.info("\n===== Begin to post job ad =====\n");
			
			
			JXTPostJobService service = new JXTPostJobService(jobAdPost.getJobBoard(), jobAdPost.getJobBoard());
		
			
			JobPostResponse response = service.postJob(jobAdPost.getOrgId(), jobAdPost.getReferenceNo(), jobAdPost.getPostingStatus(), jobAdPost.getJobContent());
			
			String message = response.getResponseCode() +" - " + response.getResponseMessage();
			
			message +="<br>Total Sent : " +  response.getSummary().getSent();
			message +="<br>Failed : " +  response.getSummary().getFailed();
			message +="<br>Updated : " +  response.getSummary().getUpdated();
			message +="<br>Inserted : " +  response.getSummary().getInserted();
			message +="<br>Archived : " +  response.getSummary().getArchived();
			
			
			model.addAttribute("message", message);
			
		
			
		}catch(Exception ex){
			
			model.addAttribute("message", ex.getMessage());
		}
		return "feedback";
	}
	
	
	@RequestMapping(method=RequestMethod.POST,value="/archiveJobAd")
	public String archiveJobAd(@ModelAttribute JobAdPost jobAdPost,ModelMap model){
		
		try{
			
			logger.info("\n===== Begin to archive job ad =====\n");
			
			
			
			JXTArchiveJobService service = new JXTArchiveJobService(jobAdPost.getJobBoard(), jobAdPost.getJobBoard());
			
			String rnoString = jobAdPost.getReferenceNo();
			String[] rnos = rnoString.split(",");
			
			JobPostResponse response = service.archiveJob(jobAdPost.getOrgId(), Arrays.asList( rnos ));
			
			String message = response.getResponseCode() +" - " + response.getResponseMessage();
			
			message +="<br>Total Sent : " +  response.getSummary().getSent();
			message +="<br>Failed : " +  response.getSummary().getFailed();
			message +="<br>Archived : " +  response.getSummary().getArchived();
			
			model.addAttribute("message", message);
			
		
			
		}catch(Exception ex){
			
			model.addAttribute("message", ex.getMessage());
		}
		return "feedback";
	}
	
	
}
