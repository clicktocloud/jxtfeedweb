package com.ctc.jxtfeedweb.domain;


public class JobAdPost {
	
	private String orgId;
	
	private String jobBoard;
	
	private String jobBoardSfApiName;
	
	private String referenceNo;
	
	private String postingStatus;
	
	//private JobListing jobListing;
	
	private String jobContent;
	
	private String advertiserId;
	
	private String username;
	
	private String password;
	
	private String namespace;

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}

	public String getReferenceNo() {
		return referenceNo;
	}

	public void setReferenceNo(String referenceNo) {
		this.referenceNo = referenceNo;
	}

	public String getPostingStatus() {
		return postingStatus;
	}

	public void setPostingStatus(String postingStatus) {
		this.postingStatus = postingStatus;
	}

	
	public String getJobContent() {
		return jobContent;
	}

	public void setJobContent(String jobContent) {
		this.jobContent = jobContent;
	}

//	public JobListing getJobListing() {
//		return jobListing;
//	}
//
//	public void setJobListing(JobListing jobListing) {
//		this.jobListing = jobListing;
//	}

	public String getJobBoard() {
		return jobBoard;
	}

	public void setJobBoard(String jobBoard) {
		this.jobBoard = jobBoard;
	}

	public String getJobBoardSfApiName() {
		return jobBoardSfApiName;
	}

	public void setJobBoardSfApiName(String jobBoardSfApiName) {
		this.jobBoardSfApiName = jobBoardSfApiName;
	}

	public String getAdvertiserId() {
		return advertiserId;
	}

	public void setAdvertiserId(String advertiserId) {
		this.advertiserId = advertiserId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getNamespace() {
		return namespace;
	}

	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}

}
