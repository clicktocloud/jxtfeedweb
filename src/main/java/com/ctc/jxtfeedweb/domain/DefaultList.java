package com.ctc.jxtfeedweb.domain;

import com.ctc.jobboard.jxt.domain.ext.AdvertiserJobTemplateLogoList;
import com.ctc.jobboard.jxt.domain.ext.ClassificationList;
import com.ctc.jobboard.jxt.domain.ext.CountryList;
import com.ctc.jobboard.jxt.domain.ext.JobTemplateList;
import com.ctc.jobboard.jxt.domain.ext.WorkTypeList;

public class DefaultList {
	private JobTemplateList jobTemplateList;
	private WorkTypeList workTypeList;
	private AdvertiserJobTemplateLogoList advertiserJobTemplateLogoList;
	private ClassificationList classificationList;
	private CountryList countryList;
	
	public JobTemplateList getJobTemplateList() {
		return jobTemplateList;
	}
	public void setJobTemplateList(JobTemplateList jobTemplateList) {
		this.jobTemplateList = jobTemplateList;
	}
	public WorkTypeList getWorkTypeList() {
		return workTypeList;
	}
	public void setWorkTypeList(WorkTypeList workTypeList) {
		this.workTypeList = workTypeList;
	}
	public AdvertiserJobTemplateLogoList getAdvertiserJobTemplateLogoList() {
		return advertiserJobTemplateLogoList;
	}
	public void setAdvertiserJobTemplateLogoList(
			AdvertiserJobTemplateLogoList advertiserJobTemplateLogoList) {
		this.advertiserJobTemplateLogoList = advertiserJobTemplateLogoList;
	}
	public ClassificationList getClassificationList() {
		return classificationList;
	}
	public void setClassificationList(ClassificationList classificationList) {
		this.classificationList = classificationList;
	}
	public CountryList getCountryList() {
		return countryList;
	}
	public void setCountryList(CountryList countryList) {
		this.countryList = countryList;
	}

}
