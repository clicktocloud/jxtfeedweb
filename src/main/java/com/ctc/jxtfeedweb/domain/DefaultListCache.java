package com.ctc.jxtfeedweb.domain;

import java.util.HashMap;
import java.util.Map;

public class DefaultListCache {
	
	
	private static Map<String, DefaultList> listCache = new HashMap<String,DefaultList>();
	
	
	
	public synchronized static DefaultList getDefaultList(String key){
		return listCache.get(key);
	}
	
	public synchronized static void addDefaultList(String key, DefaultList defaultList){
		listCache.put(key, defaultList);
	}

}
