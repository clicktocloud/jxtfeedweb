package com.ctc.jxtfeedweb.service;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ctc.jobboard.jxt.domain.ext.AdvertiserJobTemplateLogoList;
import com.ctc.jobboard.jxt.domain.ext.AreaList;
import com.ctc.jobboard.jxt.domain.ext.ClassificationList;
import com.ctc.jobboard.jxt.domain.ext.CountryList;
import com.ctc.jobboard.jxt.domain.ext.JobTemplateList;
import com.ctc.jobboard.jxt.domain.ext.LocationList;
import com.ctc.jobboard.jxt.domain.ext.SubClassificationList;
import com.ctc.jobboard.jxt.domain.ext.WorkTypeList;
import com.ctc.jobboard.jxt.service.JXTGetDefaultListService;
import com.ctc.jxtfeedweb.domain.DefaultList;
import com.ctc.jxtfeedweb.domain.DefaultListCache;


@RestController
public class BoardDefaultListService {

	@RequestMapping(value="default/list/{orgId}/{jobBoard}", method=RequestMethod.GET, produces={"application/json"})
	public DefaultList getDefaultList(@PathVariable("orgId") String orgId, @PathVariable("jobBoard") String jobBoard){
		return getDefaultList(orgId,jobBoard, "");
			
	}
	
	@RequestMapping(value="default/list/{orgId}/{jobBoard}/{namespace}", method=RequestMethod.GET, produces={"application/json"})
	public DefaultList getDefaultList(@PathVariable("orgId") String orgId, @PathVariable("jobBoard") String jobBoard, @PathVariable("namespace") String namespace){
		DefaultList list = new DefaultList();
		
		if(orgId == null || jobBoard == null){
			return list;
		}
		
		String key = orgId +":" + jobBoard;
		DefaultList cache = DefaultListCache.getDefaultList(key);
		if(cache != null ){
			return cache;
		}
		
		JXTGetDefaultListService service = new JXTGetDefaultListService(jobBoard, namespace);
		service.retrieveDefaultList(orgId);
		
		
		list.setJobTemplateList(service.getJobTemplateList());
		list.setWorkTypeList(service.getWorkTypeList());
		list.setAdvertiserJobTemplateLogoList(service.getAdvertiserJobTemplateLogoList());
		list.setClassificationList(service.getClassificationList());
		list.setCountryList(service.getCountryList());
		
		DefaultListCache.addDefaultList(key, list);
		
		return list;
		
		
	}
	

	@RequestMapping(value="default/reload/{orgId}/{jobBoard}", method=RequestMethod.GET, produces={"application/json"})
	public DefaultList reloadDefaultList(@PathVariable("orgId") String orgId, @PathVariable("jobBoard") String jobBoard){
		
		return reloadDefaultList(orgId, jobBoard, "");
	}
	
	@RequestMapping(value="default/reload/{orgId}/{jobBoard}/{namespace}", method=RequestMethod.GET, produces={"application/json"})
	public DefaultList reloadDefaultList(@PathVariable("orgId") String orgId, @PathVariable("jobBoard") String jobBoard, @PathVariable("namespace") String namespace){
		
		DefaultList list = new DefaultList();
		
		if(orgId == null || jobBoard == null){
			return list;
		}
		
		
		JXTGetDefaultListService service = new JXTGetDefaultListService(jobBoard, namespace);
		service.retrieveDefaultList(orgId);

		list.setJobTemplateList(service.getJobTemplateList());
		list.setWorkTypeList(service.getWorkTypeList());
		list.setAdvertiserJobTemplateLogoList(service.getAdvertiserJobTemplateLogoList());
		list.setClassificationList(service.getClassificationList());
		list.setCountryList(service.getCountryList());
		
		String key = orgId +":" + jobBoard;
		DefaultListCache.addDefaultList(key, list);
		
		return list;
		
		
	}
	
	@RequestMapping(value="default/getCountryList/{orgId}/{jobBoard}/{namespace}", method=RequestMethod.GET, produces={"application/json"})
	public CountryList getCountryList(@PathVariable("orgId") String orgId, @PathVariable("jobBoard") String jobBoard, @PathVariable("namespace") String namespace){
		JXTGetDefaultListService service = new JXTGetDefaultListService(jobBoard, namespace);
		
		return service.retrieveDefaultList(orgId).getCountryList();
		
		
	}
	
	@RequestMapping(value="default/getLocationList/{orgId}/{jobBoard}/{namespace}", method=RequestMethod.GET, produces={"application/json"})
	public LocationList getLocationList(@PathVariable("orgId") String orgId, @PathVariable("jobBoard") String jobBoard, @PathVariable("namespace") String namespace){
		JXTGetDefaultListService service = new JXTGetDefaultListService(jobBoard, namespace);
		
		return service.retrieveDefaultList(orgId).getLocationList();
		
		
	}
	
	@RequestMapping(value="default/getLocationList/{countryId}/{orgId}/{jobBoard}/{namespace}", method=RequestMethod.GET, produces={"application/json"})
	public LocationList getLocationList(@PathVariable("countryId") String countryId, @PathVariable("orgId") String orgId, @PathVariable("jobBoard") String jobBoard, @PathVariable("namespace") String namespace){
		JXTGetDefaultListService service = new JXTGetDefaultListService(jobBoard, namespace);
		
		return service.retrieveDefaultList(orgId).getLocationList(countryId);
		
		
	}
	
	@RequestMapping(value="default/getAreaList/{orgId}/{jobBoard}/{namespace}", method=RequestMethod.GET, produces={"application/json"})
	public AreaList getAreaList(@PathVariable("orgId") String orgId, @PathVariable("jobBoard") String jobBoard, @PathVariable("namespace") String namespace){
		JXTGetDefaultListService service = new JXTGetDefaultListService(jobBoard, namespace);
		
		return service.retrieveDefaultList(orgId).getAreaList();
		
		
	}
	
	@RequestMapping(value="default/getAreaList/{locationId}/{orgId}/{jobBoard}/{namespace}", method=RequestMethod.GET, produces={"application/json"})
	public AreaList getAreaList(@PathVariable("locationId") String locationId, @PathVariable("orgId") String orgId, @PathVariable("jobBoard") String jobBoard, @PathVariable("namespace") String namespace){
		JXTGetDefaultListService service = new JXTGetDefaultListService(jobBoard, namespace);
		
		return service.retrieveDefaultList(orgId).getAreaList(locationId);
		
		
	}
	
	
	@RequestMapping(value="default/getJobTemplateList/{orgId}/{jobBoard}/{namespace}", method=RequestMethod.GET,produces={"application/json"})
	public JobTemplateList getJobTemplateList( @PathVariable("orgId") String orgId, @PathVariable("jobBoard") String jobBoard, @PathVariable("namespace") String namespace){
		JXTGetDefaultListService service = new JXTGetDefaultListService(jobBoard, namespace);
		
		return service.retrieveDefaultList(orgId).getJobTemplateList();
	}
	

	@RequestMapping(value="default/getAdvertiserJobTemplateLogoList/{orgId}/{jobBoard}/{namespace}", method=RequestMethod.GET,produces={"application/json"})
	public AdvertiserJobTemplateLogoList getAdvertiserJobTemplateLogoList( @PathVariable("orgId") String orgId, @PathVariable("jobBoard") String jobBoard, @PathVariable("namespace") String namespace){
		JXTGetDefaultListService service = new JXTGetDefaultListService(jobBoard, namespace);
		
		return service.retrieveDefaultList(orgId).getAdvertiserJobTemplateLogoList();
	}
	
	@RequestMapping(value="default/getWorkTypeList/{orgId}/{jobBoard}/{namespace}", method=RequestMethod.GET,produces={"application/json"})
	public WorkTypeList getWorkTypeList( @PathVariable("orgId") String orgId, @PathVariable("jobBoard") String jobBoard, @PathVariable("namespace") String namespace){
		JXTGetDefaultListService service = new JXTGetDefaultListService(jobBoard, namespace);
		
		return service.retrieveDefaultList(orgId).getWorkTypeList();
	}
	
	@RequestMapping(value="default/getClassificationList/{orgId}/{jobBoard}/{namespace}", method=RequestMethod.GET,produces={"application/json"})
	public ClassificationList getClassificationList( @PathVariable("orgId") String orgId, @PathVariable("jobBoard") String jobBoard, @PathVariable("namespace") String namespace){
		JXTGetDefaultListService service = new JXTGetDefaultListService(jobBoard,namespace);
		
		return service.retrieveDefaultList(orgId).getClassificationList();
	}
	
	@RequestMapping(value="default/getSubClassificationList/{orgId}/{jobBoard}/{namespace}", method=RequestMethod.GET,produces={"application/json"})
	public SubClassificationList getSubClassificationList( @PathVariable("orgId") String orgId, @PathVariable("jobBoard") String jobBoard, @PathVariable("namespace") String namespace){
		JXTGetDefaultListService service = new JXTGetDefaultListService(jobBoard, namespace);
		
		return service.retrieveDefaultList(orgId).getSubClassificationList( );
	}
	
	@RequestMapping(value="default/getSubClassificationList/{classificationId}/{orgId}/{jobBoard}/{namespace}", method=RequestMethod.GET,produces={"application/json"})
	public SubClassificationList getSubClassificationList( @PathVariable("classificationId") String classificationId,@PathVariable("orgId") String orgId, @PathVariable("jobBoard") String jobBoard, @PathVariable("namespace") String namespace){
		JXTGetDefaultListService service = new JXTGetDefaultListService(jobBoard, namespace);
		
		return service.retrieveDefaultList(orgId).getSubClassificationList(classificationId);
	}

}
