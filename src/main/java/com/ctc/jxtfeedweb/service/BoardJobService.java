/**
 * 
 */
package com.ctc.jxtfeedweb.service;


import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ctc.jobboard.jxt.domain.ext.JobPostResponse;
import com.ctc.jobboard.jxt.service.JXTArchiveJobService;
import com.ctc.jobboard.jxt.service.JXTPostJobService;
import com.ctc.jobboard.util.JobBoardHelper;
import com.ctc.jxtfeedweb.domain.JobAdPost;
import com.ctc.jxtfeedweb.domain.ServiceResponse;




@RestController
public class BoardJobService {
	@Autowired
	private HttpServletRequest request;
	
	private static Logger logger = Logger.getLogger("com.ctc.jobboard");
	
	public boolean validate(JobAdPost jobAdPost){
		
		if(jobAdPost == null 
				|| StringUtils.isEmpty( jobAdPost.getReferenceNo())
				|| StringUtils.isEmpty( jobAdPost.getOrgId())){
			return false;
		}
		
		return true;
		
	}

	
	@RequestMapping(method=RequestMethod.POST,value="job/postJobAd", produces={"application/json"})
	public ServiceResponse postJobAd(@ModelAttribute JobAdPost jobAdPost){
	
		
		ServiceResponse result = null;
		
		if(! validate( jobAdPost )){
			return new ServiceResponse(500,"Please input correct parameters !");
		}
		
		try{
			
			logger.info("\n===== Begin to post job ad =====\n");
			
			
			
			JXTPostJobService service = new JXTPostJobService(jobAdPost.getJobBoard(), jobAdPost.getJobBoardSfApiName());
			
			JobPostResponse response = null;
			if(jobAdPost.getAdvertiserId() == null || jobAdPost.getAdvertiserId().equals("")){
				response = service.postJob(
						jobAdPost.getOrgId(), 
						jobAdPost.getReferenceNo(), 
						jobAdPost.getPostingStatus(),
						jobAdPost.getJobContent());
			}else{
				response = service.postJob(
						jobAdPost.getOrgId(), 
						jobAdPost.getReferenceNo(), 
						jobAdPost.getPostingStatus(),
						jobAdPost.getJobContent(),
						jobAdPost.getAdvertiserId(),
						jobAdPost.getUsername(),
						jobAdPost.getPassword(),
						jobAdPost.getNamespace());
			}
			
			
			
			if(response != null){
				
				String message = response.getResponseCode() +" - " + response.getResponseMessage();
				message +="\nTotal Sent : " +  response.getSummary().getSent();
				message +="\nFailed : " +  response.getSummary().getFailed();
				message +="\nUpdated : " +  response.getSummary().getUpdated();
				message +="\nInserted : " +  response.getSummary().getInserted();
				message +="\nArchived : " +  response.getSummary().getArchived();
				result=new ServiceResponse(200, message);
			}else{
				result=new ServiceResponse(500, "Unkown error : please contact to admin");
			}
			
			
		}catch(Exception ex){
			
			result=new ServiceResponse(500,"Failed to post job ad - " + ex.getMessage());
		}
		return result;
	}
	
	@RequestMapping(method=RequestMethod.POST,value="job/postJsonJobAdRequest",produces={"application/json"})
	public ServiceResponse postJsonJobAdRequest(@ModelAttribute JobAdPost jobAdPost){
		
		
		ServiceResponse result = null;
		
		if(! validate( jobAdPost )){
			return new ServiceResponse(500,"Please input correct parameters !");
		}
		
		try{
			
			logger.info("\n===== Begin to post job ad =====\n");
			
			
			
			JXTPostJobService service = new JXTPostJobService(jobAdPost.getJobBoard(), jobAdPost.getJobBoardSfApiName());
			JobPostResponse response = null;
			if(jobAdPost.getAdvertiserId() == null || jobAdPost.getAdvertiserId().equals("")){
				response = service.postJsonJob(
						jobAdPost.getOrgId(), 
						jobAdPost.getReferenceNo(),
						jobAdPost.getPostingStatus(),
						jobAdPost.getJobContent());
			}else{
				response = service.postJsonJob(
						jobAdPost.getOrgId(), 
						jobAdPost.getReferenceNo(), 
						jobAdPost.getPostingStatus(),
						jobAdPost.getJobContent(),
						jobAdPost.getAdvertiserId(),
						jobAdPost.getUsername(),
						jobAdPost.getPassword(),
						jobAdPost.getNamespace());
			}
			
		
			
			if(response != null){
				String message = response.getResponseCode() +" - " + response.getResponseMessage();
				message +="\nTotal Sent : " +  response.getSummary().getSent();
				message +="\nFailed : " +  response.getSummary().getFailed();
				message +="\nUpdated : " +  response.getSummary().getUpdated();
				message +="\nInserted : " +  response.getSummary().getInserted();
				message +="\nArchived : " +  response.getSummary().getArchived();
				result=new ServiceResponse(200, message);
			}else{
				result=new ServiceResponse(500, "Unkown error : please contact to admin");
			}
			
			
			
		}catch(Exception ex){
			
			result=new ServiceResponse(500,"Failed to post job ad - " + ex.getMessage());
			
			
		}
		return result;
	}
	
	
	@RequestMapping(method=RequestMethod.POST,value="job/postJsonJobAd", consumes={"application/json;charset=utf-8"},produces={"application/json;charset=utf-8"})
	public ServiceResponse postJsonJobAd(@RequestBody JobAdPost jobAdPost){
		
		
		
		ServiceResponse result = null;
		
		if(! validate( jobAdPost )){
			return new ServiceResponse(500,"Please input correct parameters !");
		}
		
		try{
			
			logger.info("\n===== Begin to post job ad =====\n");
			
			logger.info( "getContentType = " + request.getContentType());
			jobAdPost.setJobContent(JobBoardHelper.removeBadUTF8Chars(jobAdPost.getJobContent()));
			
			JXTPostJobService service = new JXTPostJobService(jobAdPost.getJobBoard(), jobAdPost.getJobBoardSfApiName());
			JobPostResponse response = null;
			if(jobAdPost.getAdvertiserId() == null || jobAdPost.getAdvertiserId().equals("")){
				response = service.postJsonJob(
						jobAdPost.getOrgId(), 
						jobAdPost.getReferenceNo(),
						jobAdPost.getPostingStatus(),
						jobAdPost.getJobContent());
			}else{
				response = service.postJsonJob(
						jobAdPost.getOrgId(), 
						jobAdPost.getReferenceNo(), 
						jobAdPost.getPostingStatus(),
						jobAdPost.getJobContent(),
						jobAdPost.getAdvertiserId(),
						jobAdPost.getUsername(),
						jobAdPost.getPassword(),
						jobAdPost.getNamespace());
			}
			
		
			
			if(response != null){
				String message = response.getResponseCode() +" - " + response.getResponseMessage();
				message +="\nTotal Sent : " +  response.getSummary().getSent();
				message +="\nFailed : " +  response.getSummary().getFailed();
				message +="\nUpdated : " +  response.getSummary().getUpdated();
				message +="\nInserted : " +  response.getSummary().getInserted();
				message +="\nArchived : " +  response.getSummary().getArchived();
				result=new ServiceResponse(200, message);
			}else{
				result=new ServiceResponse(500, "Unkown error : please contact to admin");
			}
			
			
			
		}catch(Exception ex){
			logger.error(ex.getMessage());
			result=new ServiceResponse(500,"Failed to post job ad - " + ex.getMessage());
		}
		return result;
	}
	
	
	@RequestMapping(method=RequestMethod.POST,value="job/archiveJobAd", consumes={"application/json"},produces={"application/json"})
	public ServiceResponse archiveJobAd(@RequestBody JobAdPost jobAdPost){
		ServiceResponse result = null;
		
		if(! validate( jobAdPost )){
			return new ServiceResponse(500,"Please input correct parameters !");
		}
		
		try{
			
			logger.info("\n===== Begin to archive job ad =====\n");
			
			
			
			JXTArchiveJobService service = new JXTArchiveJobService(jobAdPost.getJobBoard(), jobAdPost.getJobBoardSfApiName());
			
			String rnoString = jobAdPost.getReferenceNo();
			String[] rnos = rnoString.split(",");
			
			JobPostResponse response = null;
			
			if(jobAdPost.getAdvertiserId() == null || jobAdPost.getAdvertiserId().equals("")){
				
				response = service.archiveJob(jobAdPost.getOrgId(), Arrays.asList( rnos ));
				
			}else{
				response = service.archiveJob(jobAdPost.getOrgId(), 
						Arrays.asList( rnos ),
						jobAdPost.getAdvertiserId(),
						jobAdPost.getUsername(),
						jobAdPost.getPassword(),
						jobAdPost.getNamespace());
			}
			
			if(response!= null){
				String message = response.getResponseCode() +" - " + response.getResponseMessage();
				
				message +="\nTotal Sent : " +  response.getSummary().getSent();
				message +="\nFailed : " +  response.getSummary().getFailed();
				message +="\nArchived : " +  response.getSummary().getArchived();
				
				result=new ServiceResponse(200, message);
			}else{
				result=new ServiceResponse(500, "Unkown error !");
			}
			
			
			
		}catch(Exception ex){
			
			result=new ServiceResponse(500,"Failed to archive job ads - " + ex.getMessage());
		}
		return result;
	}
	
	@RequestMapping(method=RequestMethod.POST,value="job/archiveJobAdRequest")
	public ServiceResponse archiveJobAdRequest(@ModelAttribute JobAdPost jobAdPost){
		ServiceResponse result = null;
		
		if(! validate( jobAdPost )){
			return new ServiceResponse(500,"Please input correct parameters !");
		}
		
		try{
			
			logger.info("\n===== Begin to archive job ad =====\n");
			
			
			
			JXTArchiveJobService service = new JXTArchiveJobService(jobAdPost.getJobBoard(), jobAdPost.getJobBoardSfApiName());
			
			String rnoString = jobAdPost.getReferenceNo();
			String[] rnos = rnoString.split(",");
			
			JobPostResponse response = null;
			
			if(jobAdPost.getAdvertiserId() == null || jobAdPost.getAdvertiserId().equals("")){
				
				response = service.archiveJob(jobAdPost.getOrgId(), Arrays.asList( rnos ));
				
			}else{
				response = service.archiveJob(jobAdPost.getOrgId(), 
						Arrays.asList( rnos ),
						jobAdPost.getAdvertiserId(),
						jobAdPost.getUsername(),
						jobAdPost.getPassword(),
						jobAdPost.getNamespace());
			}
			
			if(response!= null){
				String message = response.getResponseCode() +" - " + response.getResponseMessage();
				
				message +="\nTotal Sent : " +  response.getSummary().getSent();
				message +="\nFailed : " +  response.getSummary().getFailed();
				message +="\nArchived : " +  response.getSummary().getArchived();
				
				result=new ServiceResponse(200, message);
			}else{
				result=new ServiceResponse(500, "Unkown error !");
			}
			
			
			
		}catch(Exception ex){
			
			result=new ServiceResponse(500,"Failed to archive job ads - " + ex.getMessage());
		}
		return result;
	}
	
	
	
}
