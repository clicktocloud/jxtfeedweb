<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="stylesheet" href="css/W3.css">

<title>JXT JOB BOARD</title>
</head>
<body>
	<header class="w3-container w3-row w3-teal">
		<h2>Feedback Message</h2>
	</header>
	<div  class="w3-col m8 l9" style="padding-top:65px;padding-bottom:30px;">
		<div class="w3-threequarter">

			

			<div class="w3-row">
				
				<div class="w3-container w3-card  w3-green w3-round w3-large" style="margin-top:100px; padding:40px;">
						
							${message}
				</div>
						
			</div>
		</div>


	</div>

	<footer class="w3-container w3-row w3-black"
		style="position: fixed; bottom: 0px; width: 100%; z-index: 2;">
		Copyright - CTC </footer>
		

 	

</body>
</html>