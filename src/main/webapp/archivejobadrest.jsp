<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src="js/angular.min.js"></script>
<script src="js/angular-route.min.js"></script>

<link rel="stylesheet" href="css/W3.css">

<title>JXT JOB POST DEMO</title>

</head>
<body ng-app="postJobApp" ng-controller="postJobCtrl">
	<header class="w3-container w3-row w3-teal">
		<h2>Post Job Advertisement:</h2>
	</header>
	<div  class="w3-col m8 l9" style="padding-top:65px;padding-bottom:30px;">
		<div class="w3-threequarter">
			<div class="w3-container w3-card" style="margin-top: 20px;margin-bottom:20px;" >{{message}}</div>
			<form class="w3-container" method="POST" ng-submit="submitPostJob()" >
				<div id="sfinfo">
					
					<label class="w3-label" for="sforgid">Org Id</label>
					<input class="w3-input" type="text" ng-model="orgId" id="sforgid" value="00D90000000gtFz"/><br/>
					
					<label class="w3-label" for="sfreferenceNo">Reference No</label>
					<input class="w3-input" type="text" ng-model="referenceNo" id="sfreferenceNo" value=""/><br/>
					
					<label class="w3-label" for="sfjobboard">Job Board</label>
					<input class="w3-input" type="text" ng-model="jobBoard" id="sfjobboard" value="JXT"/><br/>
					
					<label class="w3-label" for="sfnamespace">Namespace</label>
					<input class="w3-input" type="text" ng-model="namespace" id="sfnamespace" value=""/><br/>
					
					<label class="w3-label" for="sfjobBoardSfApiName">Job Board SF Api Name</label>
					<input class="w3-input" type="text" ng-model="jobBoardSfApiName" id="sfjobBoardSfApiName" value="JXT__c"/><br/>
					
				</div>
				<div class="w3-row">
					<label class="w3-label" >JXT Account:</label>
					<input class="w3-input" type="text" ng-model="advertiserId" />
					<input class="w3-input" type="text" ng-model="username" />
					<input class="w3-input" type="text" ng-model="password" />
				</div>
				
				<div class="w3-row">
					<input class="w3-btn w3-round-large" type="submit" name="submit" value="Post JobAd" id="postJobAd"/>
				</div>
			</form>
			</div>
	</div>
	<footer class="w3-container w3-row w3-black"
		style="position: fixed; bottom: 0px; width: 100%; z-index: 2;">
		Copyright - CTC </footer>
		
	<script>
		var mainApp = angular.module("postJobApp",[]);
		
		mainApp.controller("postJobCtrl",function($scope,$http,$httpParamSerializer){
			$scope.message = "";
			$scope.orgId = "00D90000000gtFz";
			$scope.jobBoard = "JXT";
			$scope.namespace = "";
			$scope.jobBoardSfApiName = "JXT__c";
			$scope.advertiserId = "10546";
			$scope.username = "user";
			$scope.password = "c2cTest1";
			
			
			$scope.submitPostJob = function(){
				var formDataObj = {};
				formDataObj["orgId"] = $scope.orgId;
				formDataObj["referenceNo"] = $scope.referenceNo;
				formDataObj["jobBoard"] = $scope.jobBoard;
				formDataObj["namespace"] = $scope.namespace;
				formDataObj["jobBoardSfApiName"] = $scope.jobBoardSfApiName;
				formDataObj["advertiserId"] = $scope.advertiserId;
				formDataObj["username"] = $scope.username;
				formDataObj["password"] = $scope.password;
				
				var req = {
						 method: 'POST',
						 url: '/jxtfeedweb/job/archiveJobAdRequest',
						 headers: {
							   'Content-Type': 'application/x-www-form-urlencoded'
							 },
						 data: $httpParamSerializer(formDataObj)
						};
				
				$http(req)
					.success(function(response){
						$scope.message = response.message;
					})
					.error(function(response){
						$scope.message ="Error ! <br>"+ response.statusCode + response.message;
					});
			};
			
		} );
	
	</script>
</body>
</html>