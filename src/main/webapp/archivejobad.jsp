<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<link rel="stylesheet" href="css/W3.css">

<title>JXT JOB POST DEMO</title>

</head>
<body>
	<header class="w3-container w3-row w3-teal">
		<h2>Post Job Advertisement:</h2>
	</header>
	<div  class="w3-col m8 l9" style="padding-top:65px;padding-bottom:30px;">
		<div class="w3-threequarter">
			<form class="w3-container"  enctype="multipart/form-data" method="POST" action="/jxtfeedweb/archiveJobAd">
				<div id="sfinfo">
					
					<label class="w3-label" for="sforgid">Org Id</label>
					<input class="w3-input" type="text" name="orgId" id="sforgid" value="00D90000000gtFz"/><br/><br/>
					
					<label class="w3-label" for="sfreferenceNo">Reference No</label>
					<input class="w3-input" type="text" name="referenceNo" id="sfreferenceNo" value=""/><br/><br/>
					
					<label class="w3-label" for="sfjobboard">Job Board</label>
					<input class="w3-input" type="text" name="jobBoard" id="sfjobboard" value="JXT"/><br/><br/>
					
					<label class="w3-label" for="sfjobBoardSfApiName">Job Board SF Api Name</label>
					<input class="w3-input" type="text" name="jobBoardSfApiName" id="sfjobBoardSfApiName" value="JXT__c"/><br/><br/>
					
				</div>
				
				<div class="w3-row">
					<input class="w3-btn w3-round-large" type="submit" name="submit" value="Archive JobAd" id="archiveJobAd"/>
				</div>
			</form>
			</div>
	</div>
	<footer class="w3-container w3-row w3-black"
		style="position: fixed; bottom: 0px; width: 100%; z-index: 2;">
		Copyright - CTC </footer>
</body>
</html>